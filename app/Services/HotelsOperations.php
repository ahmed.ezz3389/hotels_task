<?php
namespace App\Services;

use App\Interfaces\getHotelsInterface;
use App\Services\BestHotelsJsonAdapter;
use App\Services\TopHotelsJsonAdapter;

class HotelsOperations implements getHotelsInterface
{
    public function __construct()
    {
        $this->TopHotelsJsonAdapter = new BestHotelsJsonAdapter;
        $this->BestHotelsJsonAdapter = new TopHotelsJsonAdapter;
    }

    public function getHotels($request)
    {
        //get hotels and filter it
        $filterdBestHotels = $this->filterBestHotels($request);
        $filterdTopHotels = $this->filterTopHotels($request);

        //modify response of hotels Api
        $bestHotel = $this
            ->TopHotelsJsonAdapter
            ->getHotels($filterdBestHotels);
        $topHotel = $this
            ->BestHotelsJsonAdapter
            ->getHotels($filterdTopHotels);

        //merge any number of hotels
        return $hotels = collect($bestHotel)->merge($topHotel);
    }

    // get and filter BestHotels
    public function filterBestHotels($request)
    {
        $bestHotelPath = public_path() . "/jsons/BestHotels.json";
        $bestHotelJson = json_decode(file_get_contents($bestHotelPath) , true);
        $bestHotels = collect($bestHotelJson['BestHotels'])->where('city', $request['city'])->where('fromDate', '<=', $request['from_date'])->where('toDate', '>=', $request['to_date'])->where('numberOfAdults', $request['adults_number']);
        return $bestHotels->all();
    }

    // get and filter TopHotels
    public function filterTopHotels($request)
    {
        $topHotelPath = public_path() . "/jsons/TopHotels.json";
        $topHotelJson = json_decode(file_get_contents($topHotelPath) , true);
        $topHotels = collect($topHotelJson['TopHotels'])->where('city', $request['city'])->where('from', '<=', $request['from_date'])->where('To', '>=', $request['to_date'])->where('adultsCount', $request['adults_number']);
        return $topHotels->all();
    }
}

