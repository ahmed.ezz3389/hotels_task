<?php
namespace App\Services;

use App\Interfaces\getHotelsInterface;

class TopHotelsJsonAdapter implements getHotelsInterface
{
    /*
       The following function 
       will modify TopHotels API JSON to unified JSON  
    */
    public function getHotels($topHotels)
    {
        return collect($topHotels)->map(function ($item, $key)
        {
            return ['provider' => 'TopHotels', 'hotelName' => $item['hotelName'], 'fare' => $item['discount'] = 0 ? $item['price'] : $item['price'] - $item['price'] * $item['discount'] / 100, 'amenities' => $item['amenities'], 'rate' => strlen($item['rate']) , ];
        });
    }
}

