<?php
namespace App\Services;

use App\Interfaces\getHotelsInterface;

class BestHotelsJsonAdapter implements getHotelsInterface
{
    /*
       The following function 
       will modify BestHotels API JSON to unified JSON  
    */
    public function getHotels($bestHotels)
    {
        return collect($bestHotels)->map(function ($item, $key)
        {
            return ['provider' => 'BestHotels', 'hotelName' => $item['hotel'], 'fare' => $item['hotelFare'], 'amenities' => explode(',', $item['roomAmenities']) , 'rate' => $item['hotelRate']];
        });
    }
}

