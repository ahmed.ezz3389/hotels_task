<?php

namespace App\Interfaces;

interface getHotelsInterface
{
    public function getHotels($request);
}
