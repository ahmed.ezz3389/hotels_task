<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Interfaces\getHotelsInterface;
use App\Services\BestHotelsJsonAdapter;
use App\Services\TopHotelsJsonAdapter;
use App\Services\HotelsOperations;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(getHotelsInterface::class, function ($app) {
            return new HotelsOperations;
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
