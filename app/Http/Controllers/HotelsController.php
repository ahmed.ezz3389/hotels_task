<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Interfaces\getHotelsInterface;
use App\Http\Requests\HotelsRequest;
class HotelsController extends Controller
{
    /*
       The following function 
       will return the response front end expect 
       at first, it calls HotelRequest which has the validation of the request
       then call interface getHotelsInterface that bootstrapped in AppServiceProvider 
       which call get_hotels function that call adapters classes to modify Api response 
    */
    public function index(HotelsRequest $request, getHotelsInterface $getHotelsInterface)
    {

        $hotels = $getHotelsInterface->getHotels($request);
        $sorted_hotels = $hotels->sortByDesc('rate');
        $hotels = collect($sorted_hotels->values()
            ->all())->map(function ($item, $key)
        {
            return ['provider' => $item['provider'], 'hotelName' => $item['hotelName'], 'fare' => $item['fare'], 'amenities' => $item['amenities'], ];
        });
        return Response()->json($hotels);
    }
}

