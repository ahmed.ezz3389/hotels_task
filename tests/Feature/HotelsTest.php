<?php
namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Services\HotelsOperations;
use App\Services\BestHotelsJsonAdapter;
use App\Services\TopHotelsJsonAdapter;

class HotelsTest extends TestCase
{
    //test if Hotels Api validation working well
    public function testGetHotelsValidation()
    {
        $response = $this->get('/api/hotels', ['Accept' => 'application/json']);
        $response->assertStatus(422)
            ->assertJsonStructure(['message', 'errors' => ['from_date', 'to_date', 'city', 'adults_number'] ]);

        $response = $this->get('/api/hotels?from_date=2020-07-23&to_date=2020-07-26&city=AUH&adults_number=test', ['Accept' => 'application/json']);
        $response->assertStatus(422)
            ->assertJsonStructure(['message', 'errors' => ['adults_number'] ]);

        $response = $this->get('/api/hotels?from_date=2020-07-23&to_date=2020-26-07&city=AUH&adults_number=4', ['Accept' => 'application/json']);
        $response->assertStatus(422)
            ->assertJsonStructure(['message', 'errors' => ['to_date'] ]);

        $response = $this->get('/api/hotels?from_date=2020-23-07&to_date=2020-07-26&city=AUH&adults_number=test', ['Accept' => 'application/json']);
        $response->assertStatus(422)
            ->assertJsonStructure(['message', 'errors' => ['from_date'] ]);

    }

    //test Hotels Api and make sure of the response keys
    public function testGetHotelstest()
    {
        $response = $this->get('/api/hotels?from_date=2020-07-23&to_date=2020-07-26&city=AUH&adults_number=4', ['Accept' => 'application/json']);
        $response->assertStatus(200)
            ->assertSeeInOrder(['provider', 'hotelName', 'fare', 'amenities']);
    }

    //test filter of best hotels
    public function testFilterBestHotels()
    {
        $request = ['from_date' => "2020-07-23", 'to_date' => "2020-07-26", 'city' => 'AUH', 'adults_number' => 4];
        $HotelsOperations = new HotelsOperations;
        $filterdBestHotels = $HotelsOperations->filterBestHotels($request);
        foreach ($filterdBestHotels as $filterdBestHotel)
        {
            $this->assertEquals($request['adults_number'], $filterdBestHotel['numberOfAdults']);
            $this->assertEquals($request['city'], $filterdBestHotel['city']);
            $this->assertGreaterThan($filterdBestHotel['fromDate'], $request['from_date']);
            $this->assertGreaterThan($request['to_date'], $filterdBestHotel['toDate']);
        }
    }
    //test filter of top hotels
    public function testFilterTopHotels()
    {
        $request = ['from_date' => "2020-07-23", 'to_date' => "2020-07-26", 'city' => 'AUH', 'adults_number' => 4];
        $HotelsOperations = new HotelsOperations;
        $filterdTopHotels = $HotelsOperations->filterTopHotels($request);
        foreach ($filterdTopHotels as $filterdTopHotel)
        {
            $this->assertEquals($request['adults_number'], $filterdTopHotel['adultsCount']);
            $this->assertEquals($request['city'], $filterdTopHotel['city']);
            $this->assertGreaterThan($filterdTopHotel['from'], $request['from_date']);
            $this->assertGreaterThan($request['to_date'], $filterdTopHotel['To']);
        }
    }

    //test modify TopHotels API JSON to the unified JSON  
    public function testMapingTopHotelsJosn()
    {
        $topHotelPath = public_path() . "/jsons/TopHotels.json";
        $topHotelJson = json_decode(file_get_contents($topHotelPath) , true);
        $TopHotelsJsonAdapter = new TopHotelsJsonAdapter;
        $mappedJson = $TopHotelsJsonAdapter->getHotels(collect($topHotelJson['TopHotels']));
        foreach ($mappedJson as $topHotels)
        {
            $this->assertArrayHasKey('provider', $topHotels);
            $this->assertArrayHasKey('hotelName', $topHotels);
            $this->assertArrayHasKey('fare', $topHotels);
            $this->assertArrayHasKey('amenities', $topHotels);
            $this->assertArrayHasKey('rate', $topHotels);

        }
    }

    //test modify BestHotels API JSON to the unified JSON  
    public function testMapingBestHotelsJosn()
    {
        $bestHotelPath = public_path() . "/jsons/BestHotels.json";
        $bestHotelJson = json_decode(file_get_contents($bestHotelPath) , true);
        $BestHotelsJsonAdapter = new BestHotelsJsonAdapter;
        $mappedJson = $BestHotelsJsonAdapter->getHotels(collect($bestHotelJson['BestHotels']));
        foreach ($mappedJson as $bestHotels)
        {
            $this->assertArrayHasKey('provider', $bestHotels);
            $this->assertArrayHasKey('hotelName', $bestHotels);
            $this->assertArrayHasKey('fare', $bestHotels);
            $this->assertArrayHasKey('amenities', $bestHotels);
            $this->assertArrayHasKey('rate', $bestHotels);

        }
    }

}

